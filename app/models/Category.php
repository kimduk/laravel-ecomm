<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 07.07.14
 * Time: 10:20
 */

class Category extends Eloquent
{
    protected $fillable = array('name');

    public static $rules = array('name' => 'required|min:3');

    public function products()
    {
        return $this->hasMany('Product');
    }
} 
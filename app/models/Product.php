<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 09.07.14
 * Time: 11:28
 */

class Product extends Eloquent {
    protected $fillable = array('category_id', 'title', 'description', 'price', 'availability', 'image');

    public static $rules = array(
        'category_id' => 'required|integer',
        'title' => 'required|min:2',
        'description' => 'required|min:20',
        'price' => 'required|numeric',
        'availability' => 'integer',
        'image' => 'required|image|mimes:jpeg,bmp,png,jpg,gif'
    );

    public function category()
    {
        return $this->belongsTo('Category');
    }
} 
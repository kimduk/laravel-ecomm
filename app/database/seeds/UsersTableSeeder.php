<?php
/**
 * Created by PhpStorm.
 * User: kimduk
 * Date: 11.07.14
 * Time: 16:00
 */

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $user = new User;
        $user->firstname = 'Tim';
        $user->lastname = 'Cook';
        $user->email = 'tim@gmail.com';
        $user->password = Hash::make('tim');
        $user->telephone = '806761425448';
        $user->admin = 1;

        $user->save();
    }
} 